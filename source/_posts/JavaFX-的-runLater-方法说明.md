---
title: JavaFX 的 Platform.runLater(Runnable runnable) 方法说明
date: 2018-08-07 10:00:00
tags:
    - Java
    - JavaFX
---

JavaFX 中的图形界面是非线程安全的，只能在 UI 线程中对界面进行改动，如果在非 UI 线程更新界面会抛出 IllegalStateException。

<!-- more -->

下面的代码就是当点击按钮的时候，新起一个 Thread，然后更新 Label 的值。

```java
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox root = new VBox();
        Label label = new Label("Hello World");
        Button button = new Button("Click me");

        button.setOnAction(e -> {
            Thread thread = new Thread(() -> {
                label.setText("Hello JavaFX");
            });
            thread.start();
        });

        root.getChildren().addAll(label, button);
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
```

运行上面的代码，点击按钮后控制台输出如下内容，程序终止：

```
Exception in thread "Thread-4" java.lang.IllegalStateException: Not on FX application thread; currentThread = Thread-4
```

> 至于如何更好的在后台线程与 UI 线程之间交互，本文不予以讨论，后续会专门介绍。

鉴于前面提到的关于后台线程与 UI 线程的机制，JavaFX 中提供了 Platform.runLater 方法，以允许用户从任何线程通过调用此方法，把对 UI 的操作发布到 UI 线程中，UI 线程将会在某个时刻按照任务添加的顺序，来执行方法。由于该方法是在 UI 线程中执行，所以前提条件必然是 UI 线程已经被启动，并且没有被挂掉，另外显然在传入的 Runable 中仍然不应该执行时间过长的操作，而仅仅是一些简单的 UI 更新操作等。

在 Stack Overflow 上有个关于 runLater 的反面例子：

```java
final ProgressBar bar = new ProgressBar();
new Thread(new Runnable() {
    @Override public void run() {
    for (int i = 1; i <= 1000000; i++) {
        final int counter = i;
        Platform.runLater(new Runnable() {
            @Override public void run() {
                bar.setProgress(counter / 1000000.0);
            }
        });
    }
}).start();
```

[Platform.runLater and Task in JavaFX](https://stackoverflow.com/questions/13784333/platform-runlater-and-task-in-javafx)

上面的例子中，足足有 100000 此循环，每次循环都会建立一个新的 Runable，在 UI 线程执行这样的操作是极其可怕的。如果要进行这样的操作，应该使用如下方式：

```java
Task task = new Task<Void>() {
    @Override public Void call() {
        static final int max = 1000000;
        for (int i = 1; i <= max; i++) {
            updateProgress(i, max);
        }
        return null;
    }
};

ProgressBar bar = new ProgressBar();
bar.progressProperty().bind(task.progressProperty());
new Thread(task).start();
```

在 runLater 方法中，用户传入的 Runable 被调用以前，JavaFX 会检查之前是否有在执行的 Runable，如果有的话，会等待前序任务完成，以此实现了 runLater 的任务队列，并且保证了线程安全。