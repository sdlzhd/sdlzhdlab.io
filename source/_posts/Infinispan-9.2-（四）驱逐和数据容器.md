---
title: Infinispan 9.2 （四）驱逐和数据容器
date: 2018-04-11 09:00:00
tags:
    - Java
    - 缓存
    - Infinispan
---

Infinispan 支持驱逐条目，以防耗尽内存。驱逐通常与 Cache store 结合使用，这样在驱逐的时候不会丢失条目，因为驱逐只会从内存中驱逐条目，而不会将 Cache store 和集群中的其他节点删除。

<!-- more -->

Infinispan 支持以不同的数据格式存储数据。

> 在使用驱逐的时候，钝化是一种常用的方式，这样的话，不管是在内存中还是 cache store 中不会同时存在两份相同的条目。在 cache store 中使用钝化可以很方便的更新缓存条目，而不需要再对 cache store 的内容额外更新。

> 驱逐只在本地节点执行，而不能驱逐集群中其他节点的条目。每个节点通过运行驱逐线程来分析内容容器中的条目，并确定要驱逐的内容。驱逐不考量 JVM 的可用内存，必须设置启动驱逐的 size 大于0，再能开启驱逐。如果 size 设置过大，可能导致内存不足，每个实例都可以调整其 size 属性。

## 1 驱逐

可以在 `<*-cache />` 节点内添加 `<memory />`，或者使用编程的方式通过 MemoryCOnfigurationBuilder 来启用。

条目的驱逐都在 cache 所在的线程进行。

### 1.1 驱逐策略

可以通过以下策略控制驱逐：

- NODE - 禁用驱逐，并且假定用户不会在缓存上调用 evict。这种情况下，启用钝化，将会发出警告信息。Infinispan 默认策略为 NONE。
- MANUAL - 此选项和 NODE 很相似，但是允许用户调用 evict。这种情况下启用钝化，不再有警告信息。
- REMOVE - 驱逐旧的条目，为新的条目腾出空间。通过 Caffeine 的 TinyLFU 算法和其他的算法进行驱逐。此选项主要是为了提供高命中率和低内存开销。
- EXCEPTION - 该策略通过防止抛出 `ContainerFullException` 来创建条目。该策略仅适用于总是在第二阶段提交的事务性缓存，即不允许在第一阶段提交或者同步优化。

### 1.2 驱逐类型

驱逐类型仅在 size 设置为大于 0 的时候可用，下面的驱逐类型决定了容器何时删除条目。

- COUNT - 该类型将根据缓存中有多少条目来驱逐，一旦条目数量超过 size 设置的大小，那么将会有一个条目被移除以腾出空间。

- MEMORY - 该类型将估算每个条目在内存中占用的空间，并在所有条目的总大小大于配置的 size 时驱逐条目，该类型不适用于下文提到的 OBJECT 存储类型。

### 1.3 存储类型

Infinispan 允许用户配置其数据存储的格式。无论哪种形式，都支持相同的 Infinispan 特性，但是对于某些形式的驱逐可能是受限的。目前 Infinispan 提供了三种存储格式，分别是：

- OBJECT - 将 key/value 存储为 Java 堆中的对象。仅支持 COUNT 驱逐类型。
- BINARY - 将 key/value 作为 byte[] 存储到 Java 堆中。如果配置了缓存器编组，这种存储方式将会使用缓存器编组。支持 COUNT 和 MEMORY 驱逐类型。
- OFF-HEAP - 将 key/value 存储到非堆当中，并且以字节为单位存储。如果配置了缓存器编组，这种存储方式将会使用缓存器编组。支持 COUNT 和 MEMORY 驱逐类型。

> BINARY 和 OFF-HEAP 存储类型都违反了 equality 和 hashCode 规范，因为他们返回的结果都是 byte[] 而不是 对象实例。

### 1.4 更多默认值

默认情况下，当没有指定 `<memory />` 元素时，不会执行逐出，并使用 OBJECT 存储类型，驱逐策略为 NONE。

如果存在 `<memory />` 元素，将根据 XML 配置中提供的信息设置驱逐行为。

| size | 实例 | 驱逐行为
| - | :- | :-
| - | `<memory />` | 对象不会被驱逐
| - | `<memory strategy="MANUAL">` | 对象不会被驱逐，并且如果启用钝化，也不会出现 warn
| >0 | `<memory> <object size="100" /> </memory>` | 条目以 OBJECT 方式存储，将会驱逐对象，以保证不会超过 100 个条目
| >0 | `<memory> <binary size="100" eviction="MEMORY"/> </memory>` | 条目以 BINARY 方式存储，将会驱逐对象，以保证不会超过 100 个条目
| >0 | `<memory> <off-heap size="100" /> </memory>` | 条目存储在堆外，将会驱逐对象，以保证不会超过 100 个条目
| >0 | `<memory> <off-heap size="100" strategy="EXCEPTION" /> </memory>` | 条目存储在堆外，并且如果超过 100 个条目，将会抛出 `ContainerFullException`
| 0 | `<memory> <object size="0" /> </memory>` | 不会驱逐
| <0 | `<memory> <object size="-1" /> </memory>` | 不会驱逐

## 2 过期

过期与驱逐类似，但有一些不一样的地方。过期允许你为条目设置 lifespan 和 maxIdle。当条目过期的时候，会被视为无效并删除，删除过期的条目不会像驱逐那样钝化（如果启用钝化）。

> 与驱逐不同，过期的条目将会从内存、Cache store、集群范围内全部删除。

默认情况下，缓存中的条目是不会过期，没有 lifespan 和 maxIdle。通过缓存 API，可以在放入条目的时候附带寿命。此外还可以通过将 `<expiration />` 元素添加到 `*-cache />` 中来配置缓存寿命。

当条目过期的时候，他将驻留在数据容器或者 Cache store 中，直到用户再次请求它。还有一个过期的选项，可以给定毫秒数的时间间隔，Infinispan 将每隔这样一段时间去检查过期的条目将其删除。

### 2.1 驱逐和过期的区别

驱逐和过期都是用来清除未使用的条目，并防止内存溢出，但是存在一些差异。

对于驱逐，可以设置缓存能保存的条目的最大数量，如果条目数量超过此限制，则会根据选择的驱逐策略（LRU、LIRS 等）删除一些候选项。可以为驱逐设置钝化（条目将被驱逐到 Cache store）。

对于过期，你可以设置条目在缓存中的有效时间。可以通过设置 lifespan，或者 maxIdle 实现。

## 3 过期详解

1. 过期是一个很顶层的设置，在 XML 和 API 中都可以配置。
2. 驱逐的是本地的条目，过期是集群范围的。到期的寿命和最大空闲时间配置会随缓存条目一起复制。
3. 在复制最大空闲时间的时候，最大空闲时间过期是不集群范围内的，只有寿命才是，对于集群来说不是一致的。因此不建议在集群环境下使用 maxIdle。

### 3.1 配置

驱逐可以使用 `Configuration` bean 或 XML 文件进行配置。驱逐配置是针对缓存的。以下是一个有效的驱逐设置：

```xml
<memory>
   <object size="2000"/>
</memory>
<expiration lifespan="1000" max-idle="500" interval="1000" />
```

使用编程配置上述配置的等效代码如下：

```java
Configuration c = new ConfigurationBuilder()
               .memory().size(2000)
               .expiration().wakeUpInterval(5000l).lifespan(1000l).maxIdle(500l)
               .build();
```

### 3.2 基于内存存储的驱逐配置

如果使用自己的自定义类型（这是很常见的），基于内存的驱逐可能需要一些额外的配置选项。因为在这种情况下，Infinispan 无法估计类的内存使用情况，所以当使用基于内存的驱逐时，您需要使用 `storeAsBinary`。

```xml
<!-- <Enable memory based eviction with 1 GB /> -->
<memory>
   <binary size="1000000000" eviction="MEMORY" />
</memory>
```

或者通过编程的方式：

```java
Configuration c = new ConfigurationBuilder()
               .memory()
               .storageType(StorageType.BINARY)
               .evictionType(EvictionType.MEMORY)
               .size(1_000_000_000)
               .build();
```

### 3.3 默认值

驱逐默认是被禁用的。使用如下默认值：

- size 如果未指定，则使用 -1，这意味着无限制条目。
- 0 表示没有条目，驱逐线程将保持缓存为空。

过期的 lifespan 和 maxIdle 默认都为 -1。

### 3.4 使用过期

过期允许您设置存储在缓存中的每个 key/value 对的 lifespan 或 maxIdle。如上文所述，可以通过 XML 或 编程的方式配置过期。针对具体的条目的 lifespan 和 maxIdle 设置都将覆盖宽泛的缓存定义。

例如，假定以下配置：

```xml
<expiration lifespan="1000" />
```

并且存在这样的代码：

```java
// 此条目将在 1000 毫秒过期
cache.put("pinot noir", pinotNoirPrice);

// 此条目将在 2000 毫秒过期
cache.put("chardonnay", chardonnayPrice, 2, TimeUnit.SECONDS);

// 此条目在最后一次访问后，1000 毫秒后过期
cache.put("pinot grigio", pinotGrigioPrice, -1,
          TimeUnit.SECONDS, 1, TimeUnit.SECONDS);

// 此条目在最后一次访问后，1000 毫秒后过期, 或者
// 在 5000 毫秒后过期
cache.put("riesling", rieslingPrice, 5,
          TimeUnit.SECONDS, 1, TimeUnit.SECONDS);
```

## 4 其他的过期配置

过期的核心是 `ExpirationManager` 类。

ExpirationManager 主要是驱动过期线程定期清除数据容器中的过期条目。如果过期线程被禁用（wakeupInterval 被设置为 -1），则可以使用 ExprationManager 手动处理到期。processExpiration() 可能是被应用程序中定期运行的另一个线程调用。

ExpirationManager 通过以下方式处理过期时间：

- 让数据容器清除过期条目
- 让 cache stores(如果有) 清除过期条目