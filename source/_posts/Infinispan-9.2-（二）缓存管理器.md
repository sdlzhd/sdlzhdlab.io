---
title: Infinispan 9.2 （二）缓存管理器”
date: 2018-04-09 14:00:00
tags:
    - Java
    - 缓存
    - Infinispan
---

CacheManager 是 Infinispan 的主要入口， 你可以通过 CacheManager

- 配置并获取缓存
- 管理和监控节点
- 在集群中执行代码
- 其他

根据 Infinispan 的使用方式，需要选择不同的 CacheManager，如果将 Infinispan 切入到应用程序则需要使用 `EmbeddedCacheManager`，如果是远程使用，则使用 `RemoteCacheManager`。尽管这两种 Manager 有一些一样的方法和属性，但是请注意他们的使用场景，下文主要讨论**嵌入式实现**。

<!-- more -->

CacheManagers 是重量级对象，并且大部分情况下每个 JVM 应该仅存在一个 CacheManger。

创建 Cachemanager 的最简单方式：

```java
EmbeddedCacheManager manager = new DefaultCacheManager();
```

以上代码创建了一个使用本地模式的 CacheManger，并非集群模式。CacheManager 有自己的生命周期，默认的构造函数会调用 start() 方法。我们可以重写一个构造函数，不去调用 start()，但是请记住，CacheManager 必须执行 start() 以后，才能创建缓存实例。

CacheManager 一旦创建，应该可以在整个应用程序中通过某种形式被任意组件使用。

当使用完 CacheManger 时，必须调用 stop 停止它，以释放资源。

```java
manager.stop();
```

这会确保其作用范围内的缓存都被正常停止，线程池被关闭。如果在集群模式下，这将优雅的离开集群。

## 1 配置

Infinispan 提供声明式和程序式配置。

### 1.1 以声明方式配置缓存

声明式配置采用 XML 的形式。

声明式配置的每一项都可以通过代码来实现。实际上，声明式配置在幕后也是通过调用编程式配置的 API 实现的。所以也可以组合使用两种配置方式。比如，可以读取 XML 配置文件得到 CacheManager，并且在运行的时候以编程的方式再调整配置。或者通过 XML 定义特定的静态配置作为 CacheManager 的配置模板，然后根据需要增加某些配置。

在 Infinispan 中有两个主要配置项：`global` 和 `cache`。

**Global 配置**

Global 配置定义了 EmbeddedCacheManager 创建的所有缓存实例之间共享的全局设置。例如线程池、序列化/编组设置、传输和网络设置、JMX域的共享资源等。

**Cache 配置**

Cache 配置用来配置 Cache 实例，包括驱逐、锁、事务、集群、持久化等。可以根据需要指定任意数量的 cache 配置，其中一个可以被指定为默认缓存，通过 `CacheManager.getCache()` 获取该实例，其余的则通过 `CacheManager.getCache(name)` 获取。

无论何时指定，指定的缓存都会继承默认缓存的配置，同时也可以覆盖继承的设置。Infinispan 提供了非常灵活的继承机制，可以定义配置模板，并根据需要覆盖特定参数。

> 嵌入式和服务器的配置使用不同的模式，但是 Infinispan 尽力保持他们兼容，以便在两种模式之间迁移

Infinispan 的主要目标是实现零配置。配置文件中，仅仅包含 `<infinispan />` 便可以开始使用，下面的配置文件全部使用了有效的默认值。

*infinispan.xml*
```xml
<infinispan />
```

这会提供最基本的本地模式,为了通过 XML 配置文件创建 CacheManager，一般为 DefaultCacheManager 指定配置文件的位置。一旦配置文件被读取，就可以获得默认缓存实例。

```java
EmbeddedCacheManager manager = new DefaultCacheManager("infinispan.xml");
Cache defaultCache = manager.getCache();
```

如果 XML 中有其他的命名的缓存实例，可以通过以下代码获取：

```java
Cache namedCache = manager.getCache("namedCache");
```

默认缓存的名字在 XML 文件中的 `<cache-container>` 元素中定义，可以使用 `<local-cache>`、`<distributed-cache>`、`<invalidation-cache>` 或 `<replicated-cache>` 元素来定义其他缓存。

以下示例是 Infinispan 支持的每种缓存类型的最简单的配置：

```xml
<infinispan>
   <cache-container default-cache="local">
      <transport cluster="mycluster"/>
      <local-cache name="local"/>
      <invalidation-cache name="invalidation" mode="SYNC"/>
      <replicated-cache name="repl-sync" mode="SYNC"/>
      <distributed-cache name="dist-sync" mode="SYNC"/>
   </cache-container>
</infinispan>
```

#### Cache 配置模板

如上文所述，Infinispan 支持配置模板。配置模板可以在多个缓存之间共享，它可以是完整的配置或者仅声明部分配置，或作为更复杂的配置的基础。

下面这个示例显示如何使用名为 local-template 的配置来定义名为 local 的缓存：

```xml
<infinispan>
   <cache-container default-cache="local">
      <!-- template configurations -->
      <local-cache-configuration name="local-template">
         <expiration interval="10000" lifespan="10" max-idle="10"/>
      </local-cache-configuration>

      <!-- cache definitions -->
      <local-cache name="local" configuration="local-template" />
   </cache-container>
</infinispan>
```

模板可以继承之前定义的模板，然后可以扩充或覆盖配置。

```xml
<infinispan>
   <cache-container default-cache="local">
      <!-- template configurations -->
      <local-cache-configuration name="base-template">
         <expiration interval="10000" lifespan="10" max-idle="10"/>
      </local-cache-configuration>

      <local-cache-configuration name="extended-template" configuration="base-template">
         <expiration lifespan="20"/>
         <memory>
            <object size="2000"/>
         </memory>
      </local-cache-configuration>

      <!-- cache definitions -->
      <local-cache name="local" configuration="base-template" />
      <local-cache name="local-bounded" configuration="extended-template" />
   </cache-container>
</infinispan>
```

上面的例子中，`base-template` 定义了一个具有特定到期配置的本地缓存，`extended-template` 继承自 `base-template`，并且只覆盖了过期设置（其余的属性都被继承），并且添加了一个 `memory` 元素。最后定义了两个 Cache，local 使用 `base-template` 模板，local-bounded 使用 `extended-template` 模板。

> 对于多值元素，继承是可加的，即子配置将合并来自父级和自己的属性

#### Cache 配置通配符

将模板用于 Cache 的另一种方式是在模板名称中使用通配符，例如：`basecache*`。Cache 的名称与模板通配符匹配的任何缓存都将继承该模板的配置。

```xml
<infinispan>
    <cache-container>
        <local-cache-configuration name="basecache*">
            <expiration interval="10500" lifespan="11" max-idle="11"/>
        </local-cache-configuration>
        <local-cache name="basecache-1"/>
        <local-cache name="basecache-2"/>
    </cache-container>
</infinispan>
```

上文中 Cache 名为 `basecache-1` 和 `basecache-2` 的都使用 `basecache*` 的配置。当以编程的方式检索未定义的 Cache 的时候，也将使用此配置。

> 如果缓存名称匹配多个通配符，即它不明确，则会抛出异常。

#### 配置参考
有关声明式配置的更多详细信息，请参阅 [configuration reference](http://docs.jboss.org/infinispan/9.2/configdocs/)。如果使用 XML 编辑工具，可以使用 Infinispan [schema](http://infinispan.org/schemas/infinispan-config-9.2.xsd) 来提供帮助。

### 1.2 以编程方式配置缓存

编程配置 Infinispan 是以 CacheManager 和 ConfigurationBuilder API 为主。虽然 Infinispan 的所有配置都可以通过编程来设置，但是最常用的方式是以 XML 配置，然后在运行的时候（如果需要）再以编程的方式调整特定的配置。

```java
EmbeddedCacheManager manager = new DefaultCacheManager("my-config-file.xml");
Cache defaultCache = manager.getCache();
```

我们假设以编程的方式配置新的同步复制缓存。首先使用 ConfigurationBuilder 帮助程序对象创建 Configuration 对象的实例，并将缓存模式设置为同步复制，最后将这个配置应用到 manager 上。

```java
Configuration c = new ConfigurationBuilder().clustering().cacheMode(CacheMode.REPL_SYNC).build();

String newCacheName = "repl";
manager.defineConfiguration(newCacheName, c);
Cache<String, String> cache = manager.getCache(newCacheName);
```

默认 Cache 配置可用作创建新缓存的起点。例如，我一说 infinispan-config-file.xml 指定一个默认的复制 Cache，并且希望这个 Cache 有 L1 的寿命，同时继承默认 Cache 的其他配置。首先，将是读取 XML 文件获取默认配置实例，并使用 ConfigurationBuilder 在新的 Configuration 对象上构造和修改缓存模式和 L1 寿命。，最后将这个配置应用到 manager 上。

```java
EmbeddedCacheManager manager = new DefaultCacheManager("infinispan-config-file.xml");
Configuration dcc = manager.getDefaultCacheConfiguration();
Configuration c = new ConfigurationBuilder().read(dcc).clustering().cacheMode(CacheMode.DIST_SYNC).l1().lifespan(60000L).build();
 
String newCacheName = "distributedWithL1";
manager.defineConfiguration(newCacheName, c);
Cache<String, String> cache = manager.getCache(newCacheName);
```

只要基本配置时默认缓存，上面的代码可以很好地运行。然而，如果基本配置是其他名字的缓存，那么如何基于其他定义的缓存定义新的配置？上面的代码为例，假设不使用默认缓存，而是使用名字为 replicatedCache 的缓存，可以使用如下代码：

```java
EmbeddedCacheManager manager = new DefaultCacheManager("infinispan-config-file.xml");
Configuration rc = manager.getCacheConfiguration("replicatedCache");
Configuration c = new ConfigurationBuilder().read(rc).clustering().cacheMode(CacheMode.DIST_SYNC).l1().lifespan(60000L).build();
 
String newCacheName = "distributedWithL1";
manager.defineConfiguration(newCacheName, c);
Cache<String, String> cache = manager.getCache(newCacheName);
```

更多信息请参阅 [CacheManager]() [ConfigurationBuilder](https://docs.jboss.org/infinispan/9.2/apidocs/org/infinispan/configuration/cache/ConfigurationBuilder.html)、[Configuration](https://docs.jboss.org/infinispan/9.2/apidocs/org/infinispan/configuration/cache/Configuration.html)、[GlobalConfiguration](https://docs.jboss.org/infinispan/9.2/apidocs/org/infinispan/configuration/global/GlobalConfiguration.html)。

#### ConfigurationBuilder API

上文展示了如何结合声明式配置和编程式配置，但是完全可以不通过 XML 先载入配置。利用 ConfigurationBuilder 的链式 API 可以写出可读性很高的编程式配置声明。此方法可用于 Global 配置和 Chache 配置。GlobalConfiguration 对象使用 GlobalConfigurationBuilder 构造，而 Configuration 对象使用 ConfigurationBuilder 构建。让我们来看看使用此 API 配置 Global 和 Cache 级别的配置项：

```java
GlobalConfiguration globalConfig = new GlobalConfigurationBuilder().transport()
        .defaultTransport()
        .clusterName("qa-cluster")
        .addProperty("configurationFile", "jgroups-tcp.xml")
        .machineId("qa-machine").rackId("qa-rack")
      .build();
```

有时候可能还需要在 Cache 级别启用全局 JMX 统计信息的收集或获取有关传输的信息。要启用全局 JMX 统计信息，只需要执行：

```java
GlobalConfiguration globalConfig = new GlobalConfigurationBuilder()
  .globalJmxStatistics()
    .cacheManagerName("SalesCacheManager")
    .mBeanServerLookup(new JBossMBeanServerLookup())
  .build();
```

有一些 Infinispan 功能由一组线程池来完成，这些线程池执行程序的配置也可以在 Global 级别的配置完成。例如：

```java
GlobalConfiguration globalConfig = new GlobalConfigurationBuilder()
   .replicationQueueThreadPool()
     .threadPoolFactory(ScheduledThreadPoolExecutorFactory.create())
  .build();
```

不仅可以配置 Globel 配置，还可以配置 Cache 级别的配置，例如集群模式：

```java
Configuration config = new ConfigurationBuilder()
  .clustering()
    .cacheMode(CacheMode.DIST_SYNC)
    .sync()
    .l1().lifespan(25000L)
    .hash().numOwners(3)
  .build();
```

也可以配置驱逐和过期：

```java
Configuration config = new ConfigurationBuilder()
           .memory()
             .size(20000)
          .expiration()
             .wakeUpInterval(5000L)
             .maxIdle(120000L)
           .build();
```

应用程序还可能会与 JTA 边界内的 Infinispan Cache 进行交互，并且需要配置事务层和锁。在与事务性缓存进行交互时，你可能希望启用启发式回滚的结果，并且如果这样做的话，则通常也希望启用 JMX 管理和统计信息收集：

```java
Configuration config = new ConfigurationBuilder()
  .locking()
    .concurrencyLevel(10000).isolationLevel(IsolationLevel.REPEATABLE_READ)
    .lockAcquisitionTimeout(12000L).useLockStriping(false).writeSkewCheck(true)
    .versioning().enable().scheme(VersioningScheme.SIMPLE)
  .transaction()
    .transactionManagerLookup(new GenericTransactionManagerLookup())
    .recovery()
  .jmxStatistics()
  .build();
```

使用链式 API 配置 Cache store 也很简单：

```java
Configuration config = new ConfigurationBuilder()
   .persistence().passivation(false)
   .addSingleFileStore().location("/tmp").async().enable()
   .preload(false).shared(false).threadPoolSize(20).build();
```

#### 高级编程配置

流式 API 也可以用来配置一些更高级，甚至奇特的选项，例如高级的外部化：

```java
GlobalConfiguration globalConfig = new GlobalConfigurationBuilder()
  .serialization()
    .addAdvancedExternalizer(998, new PersonExternalizer())
    .addAdvancedExternalizer(999, new AddressExternalizer())
  .build();
```

或者添加自定义的拦截器

```java
Configuration config = new ConfigurationBuilder()
  .customInterceptors().addInterceptor()
    .interceptor(new FirstInterceptor()).position(InterceptorConfiguration.Position.FIRST)
    .interceptor(new LastInterceptor()).position(InterceptorConfiguration.Position.LAST)
    .interceptor(new FixPositionInterceptor()).index(8)
    .interceptor(new AfterInterceptor()).after(NonTransactionalLockingInterceptor.class)
    .interceptor(new BeforeInterceptor()).before(CallInterceptor.class)
  .build();
```

有关各个配置选项的信息，请查看 [configuration guide](http://docs.jboss.org/infinispan/9.2/configdocs/)。

### 1.3 配置迁移工具

为了使嵌入式模式与服务器模式使用的架构保持一致，Infinispan 的配置格式从 6.0 开始发生了变化。因此，升级到Infinispan 7.x 或更高的版本得时候，应使用对应发行版中提供的配置转换器。只需要通过命令行调用它，将旧的配置文件作为第一个参数，将转换后的文件名称作为第二个参数。

在 Unix/Linux/macOS 上转换：

```bash
bin/config-converter.sh oldconfig.xml newconfig.xml
```

在 Windows 上转换：

```bash
bin\\config-converter.bat oldconfig.xml newconfig.xml
```

### 1.4 集群配置

Infinispan 在集群模式下使用 JGroups 进行网络通信。Infinispan 自带一个预先配置好的 JGroups 堆栈，以使得你可以轻松地启动集群配置。

#### 使用外部 JGroups 文件

如果通过编程的方式配置缓存，则只需执行以下操作：

```java
GlobalConfiguration gc = new GlobalConfigurationBuilder()
   .transport().defaultTransport()
   .addProperty("configurationFile", "jgroups.xml")
   .build();
```

如果使用 XML 来配置 Infinispan，请使用：

```xml
<infinispan>
  <jgroups>
     <stack-file name="external-file" path="jgroups.xml"/>
  </jgroups>
  <cache-container default-cache="replicatedCache">
    <transport stack="external-file" />
    <replicated-cache name="replicatedCache"/>
  </cache-container>

  ...

</infinispan>
```

上述的两种方式，Infinispan 首先在 classpath 中寻找 jgroups.xml，如果在 classpath 没有找到，再查找绝对路径。

#### 使用预配置的 JGroups 文件

Infinispan 自带一些不同的 JGroups 文件（在 infinispan-core.jar 中），这意味着，默认情况下它们已经在 pathclass 中。只需要指定文件名即可，例如上面的 jgroups.xml，请指定 `/default-configs/default-jgroups-tcp.xml`。

有以下这些可用的配置文件：

- default-jgroups-udp.xml - 使用 UDP 进行传输，并启用 UDP 组播来发现节点。通常适用于较大的（超过 100 个节点）集群，或如果你正在使用 replication 模式、 invalidation 模式。最大限度的减少开启 Sockets 的数量。
- default-jgroups-tcp.xml - 使用 TCP 进行传输，并启用 UDP 组播来发现节点。最好用于使用 distribution 模式的较小的（小于 100 个节点）集群，因为 TCP 作为点对点协议更高效。
- default-jgroups-ec2.xml - 使用 TCP 进行传输，并使用 S3_PING 来发现节点。适用于 UDP 组播不可用的时候，例如 Amazon EC 2.
- default-jgroups-kubernetes.xml - 使用 TCP 进行传输，并使用 KUBE_PING 来发现节点。适用于 UDP 组播不总是可用的 Kubernetes 和 OpenShift。

#### 调整 JGroups 设置

上述的设置无需编辑 XML 本身，即可进行调整。在启动时将某些系统属性传递给 JVM 可以改变其中的一些设置。下面的表格显示了可以用这种方式调整哪些配置。

```bash
java -cp ... -Djgroups.tcp.port=1234 -Djgroups.tcp.address=10.11.12.13
```

*default-jgroups-udp.xml*

System Property | Description | Default | Required?
- | :-: | :-: | -:
jgroups.udp.mcast_addr | UDP 组播的 IP 地址（用于通讯和发现），必须是有效的 D 类 IP 地址，适用于 IP 组播 | 228.6.7.8 | No
jgroups.udp.mcast_port | UDP 组播的 Socket 的端口 | 46655 | No
jgroups.udp.ip_ttl | 指定 UPD 组播数据包的生存时间（TTL）。这里的值是指数据包被丢弃之前允许的网络跳数 | 2 | No

*default-jgroups-tcp.xml*

System Property | Description | Default | Required?
- | :-: | :-: | -:
jgroups.tcp.address | TCP 传输的 IP 地址 | 127.0.0.1 | No
jgroups.tcp.port | TCP Socket 的端口 | 7800 | No
jgroups.udp.mcast_addr | UDP 组播的 IP 地址（用于发现），必须是有效的 D 类 IP 地址，适用于IP组播 | 228.6.7.8 | No
jgroups.udp.mcast_port | UDP 组播的 Socket 的端口 | 46655 | No
jgroups.udp.ip_ttl | 指定 UPD 组播数据包的生存时间（TTL）。这里的值是指数据包被丢弃之前允许的网络跳数 | 2 | No

*default-jgroups-ec2.xml*

System Property | Description | Default | Required?
- | :-: | :-: | -:
jgroups.tcp.address | TCP 传输的 IP 地址 | 127.0.0.1 | No
jgroups.tcp.port | TCP Socket 的端口 | 7800 | No
jgroups.s3.access_key | 访问 Amazon S3 Bucket 的 access_key | | No
jgroups.s3.secret_access_key | 访问 Amazon S3 Bucket 的 secret_access_key | | No
jgroups.s3.bucket | 要使用的 Amazon S3 Bucket 的名称。必须是唯一的，并且必须已经存在 | | No

*default-jgroups-kubernetes.xml*

System Property | Description | Default | Required?
- | :-: | :-: | -:
jgroups.tcp.address | TCP 传输的 IP 地址 | eth0 | No
jgroups.tcp.port |TCP Socket 的端口 | 7800 | No

#### 了解更多

JGroups还支持更多的系统属性覆盖，详细信息可以在此页面上找到：[SystemProps](http://www.jgroups.org/manual4/index.html#SystemProperties)。

另外，Infinispan 自带的 JGroups 配置文件旨在作为启动和运行的起点。但是，通常情况下，还需要进行一些微调，以便获取更好的网络性能，为此之后应该读一下 [JGroups manual](http://jgroups.org/manual/html/protlist.html)，其中详细介绍了如何配置您的 JGroups 配置文件中看到的每个协议。

## 2 获取缓存

