---
title: Infinispan 9.2 （五）持久化
date: 2018-04-12 09:00:00
tags: 
    - Java
    - 缓存
    - Infinispan
---

持久化允许你配置一个外部（永久）存储作为 Infinispan 默认内存存储的补充。使用外部存储的原因可能有以下几点：

- 提高可用性。内存中的数据是易失性的，使用外部存储可以使缓存中的信息存储更长的时间。

- 穿透（Write-through）。在应用程序和外部存储之间加入 Infinispan 作为缓存。

- 数据溢出。通过使用驱逐与钝化，可以将“热”数据存储在内存中，将不常用的数据溢出存储到外部存储中。

<!-- more -->

与外部存储的集成通过以下 SPI(串行外设接口) 完成：CacheLoader、CacheWriter、AdvancedCacheLoader 和 AdvancedCacheWriter。

这些 SPI 支持以下功能：

- 与 [JSR-107](https://jcp.org/en/jsr/detail?id=107) 一致。CacheWrite 和 CacheLoader 的接口类似于 JSR-107 的写入器和载入器，这有助于兼容其他的缓存。

- 简化事务集成。所有的锁都可以由 Infinispan 自动处理，不需要使用者去关心并发访问 Store。尽管并发的写入同样的 key 不会发生（取决于使用的锁机制），但是开发者应该考虑并发的情况，并进行相应的编码处理。

- 并行迭代。现在可以并行的使用多个线程遍历存储的条目。

- 减少序列化。这可以减少 CPU 的使用率。新 API 以序列化格式公开存储的条目。如果从外部存储中获取条目，以供给远程使用，我们将不再进行反序列化，而是直接传输序列化后的条目。

## 1 配置

Store 可以通过链式 API 配置。Cache 将按照顺序查找所指定的 CacheLoader，直到找到有效的且非空的数据。当执行写操作时，Cache 所有的 CacheWriter 都将执行写入，除非指定某个 CacheWriter 的 ignoreModeifications 属性被设置为 true。

> *实现 CacheWriter 和 CacheLoader*
> 允许并且推荐为 Store 同时实现 CacheWriter 和 CacheLoader 接口。这样 Store 既能读又能写（假设 `read-only = false`）。

这是一个配置 Store 的例子：

```xml
<local-cache name="myCustomStore">
    <persistence passivation="false">
        <store
            class="org.acme.CustomStore"
            fetch-state="false" preload="true" shared="false"
            purge="true" read-only="false" singleton="false">

            <write-behind modification-queue-size="123" thread-pool-size="23" />

            <property name="myProp">${system.property}</property>
        </store>
    </persistence>
</local-cache>
```

配置选项说明：

- passivation(默认为 false) 会影响 Infinispan 和 Loaders 的交互，已经在缓存钝化部分进行了说明。

- class 定义了 store 的类，并且必须实现 CacheLoader, CacheWriter，或者两者同时实现。

- fetch-state(默认为 false) 决定了在加入集群的时候是否获取 Cache 的持久化状态。该配置的目的是获取缓存的持久化状态，并将其应用于本地的 Cache。如果缓存存储配置为共享，fetch-state 将被忽略，因为此时不同的节点将访问相同的数据。只有一个 Cache Loader 可以将此属性设置为 true，否则在启动的时候将会引发配置异常。

- preload(默认为 false) 开启此选项将会在 Cache 启动的时候，把存储在 Cache Loader 中的数据预先加载到内存。当启动后立刻需要 Cache Loader 中的数据，不希望由于延迟加载数据而导致缓存操作延迟的时候，这将是很有用的选项。但是此功能会影响启动时间，损失一定的性能。预加载是发生在本地内存的，因此加载的任何数据都是存储在此节点的。预加载的数据不会复制和分发。此外，Infinispan 仅预载驱逐中设置的最大条目数。

- shared(默认为 false) 

- purge

- read-only

- max-batch-size

- write-behind

- singleton

- additional