---
title: Infinispan 9.2 （一）介绍
date: 2018-04-08 14:00:00
tags:
    - Java
    - 缓存
    - Infinispan
---

这是 Infinispan 的官方指南，这份文档包含了 Infinispan 几乎所有的细节。

> 如果你对 Infinispan 毫不了解，建议先去阅读 [Quickstarts](http://infinispan.org/documentation/)。

[常见问题](http://infinispan.org/docs/stable/faqs/faqs.html)和[术语表](http://infinispan.org/docs/stable/glossary/glossary.html)也是有很有用的文档。

## 1 Infinispan 是什么

Infinispan 是一个分布式的基于内存的 Key/Value 存储，使用 Apache License 2.0 许可。它即可以作为内嵌的 Java 库，也可以通过各种协议（Hot Rod, REST, Memcached and WebSockets）用作远程访问的编程语言无关的服务。它可以提供高级功能，如事务、事件、查询和分布式处理，以及与 JCache API、CDI、Hibernate、WildFly、Spring Cache、Spring Session、Lucene、Spark、Hadoop 等框架集成。

<!-- more -->

## 2 为什么使用 Infinispan

### 2.1 用作本地缓存

Infinispan 的主要用途是为需要频繁使用的数据提供告诉的内存缓存。假设有一个比较慢的数据源（数据库、Web 服务、文本文件等），你可以在内存中加载部分或者全部数据，这样就可以通过内存来访问他们。Infinispan 比简单的 ConcurrentHashMap 更棒，它提供了诸如到期、驱逐等额外的高级功能。

### 2.2 用作集群缓存

如果你的数据不适合单个节点，或者你想要使应用程序的多个实例的缓存无效，Infinispan 可以水平扩展至数百个节点。

### 2.3 用作应用程序的集群构建模块

如果需要应用程序支持集群，通过集成 Infinispan，可以访问变更消息、集群通信和集群执行等功能。

### 2.4 用作远程缓存

如果你希望能够独立于应用程序扩展缓存层，或者需要将数据提供给不同的应用程序，甚至可能使用不同的语言/平台，请使用 Infinispan Server 及其各种客户端。

### 2.5 用作 Data Grid

在 Infinispan 中放置的数据不一定是临时的，可以使用 Infinispan 作为主要的数据存储，并通过其强大的功能，如事务、通知、查询、分布式执行、分布式流等来快速的处理数据。

### 2.6 用作跨地理位置的数据备份

Infinispan支持群集之间的复制，允许跨地理位置远程备份数据。