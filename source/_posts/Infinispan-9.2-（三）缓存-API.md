---
title: Infinispan 9.2 （三）缓存 API
date: 2018-04-10 09:00:00
tags:
    - Java
    - 缓存
    - Infinispan
---

## Cache API

Infinispan 的缓存通过 Cache API 进行操作。

Cache 公开了用于添加，检索和删除条目的简单方法，包括 JDK 的ConcurrentMap 接口公开的原子操作。根据所使用的缓存模式，调用这些方法将触发许多事情，甚至包括将条目复制到远程节点或从远程节点中查找条目。

> 对于简单的用法，使用 Cache API 应该与使用 JDK Map API 没有区别，因此从基于 Map 的简单内存缓存迁移到 Infinispan 的 Cache 非常容易。

### 某些 Map API 的性能问题

Map 接口中的一些方法，例如 size()，values()，keySet()，entrySet()，在 Infinispan 中使用的时候，可能会对性能产生较大的负面影响，尤其是在分布式环境中，因此这些方法仅限应用于信息或者调试目的。

### 永久数据和临时数据

除了简单的操作条目外，Infinispan 的 Cache API 还可以附加失效率。例如，使用 `put(key, value)`，将创建一个永久的条目，这会永远的存在于缓存中，直到它被删除（或者因为内存不足被驱逐）。但是如果使用 `put(key, value, lifespan, timeunit)` 将数据放置于缓存中，那么他将会在指定的时间后过期。

除了 lifespan，Infinispan 还支持将 maxIdele 作为确定的到期时间。

### 使用临时数据的示例

### putForExternalRead 操作

Infinispan 的 Cache 类包含一个叫做 putForExternalRead 的不同于 put 的方法，当 Infinispan 用作其他存储的临时缓存的时候这个方法时非常有用的。在大量的读取的情况下，缓存的争用不应该阻碍要进行的其他操作，换句话说，缓存应该是一种优化，而不是阻碍。

为了实现这一点，putForExternalRead 在缓存中不存在这个 Key 的时候，会和 put 一样，但是如果另一个线程正在试图存储相同的 Key 的时候，putForExternalRead 则会无声的快速失败。在这种情况下，缓存是一种优化系统的方法，而且缓存的失败不影响正在进行的事务。因此失败的处理方式是特殊的。putForExternalRead 是一种快速操作，因为无论是否成功，他都不会等待锁的释放，而是立刻 return。

通过下面这个例子，来看一下如何使用此方法。假设一个 Person 缓存，每个条目都由 PersonId 作为 Key，这些数据来源于一个单独的数据存储。

```java
// Id of the person to look up, provided by the application
PersonId id = ...;

// Get a reference to the cache where person instances will be stored
Cache<PersonId, Person> cache = ...;

// First, check whether the cache contains the person instance
// associated with with the given id
Person cachedPerson = cache.get(id);

if (cachedPerson == null) {
   // The person is not cached yet, so query the data store with the id
   Person person = dataStore.lookup(id);

   // Cache the person along with the id so that future requests can
   // retrieve it from memory rather than going to the data store
   cache.putForExternalRead(id, person);
} else {
   // The person was found in the cache, so return it to the application
   return cachedPerson;
}

```