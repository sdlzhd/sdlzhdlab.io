---
title: Java 文档注释
date: 2018-08-10 16:00:00
tags:
    - Java
---

Java 中有三种注释，分别为：

- 单行注释 `// 注释内容`
- 多行注释 `/* ...注释内容... */`
- 文档注释 `/** ...注释内容... */`

JDK 中有个叫做 javadoc 的工具，用来从源文件中提取出文档注释，并生成一份 HTML 文档，我们看到的官方的 Java API 文档就是由 javadoc 生成的。

javadoc 从以下几个地方抽取**文档注释**：

- package
- public 和 protect 修饰的 Class、Interface
- public 和 protect 修饰的 构造器和方法
- public 和 protect 修饰的域

<!-- more -->

一般文档注释的基本内容如下：

```java
/**
 * 概要性内容，描述被注释着的基本作用
 * @author 由 @ 开始的标记信息
 * 其他内容
 */
```

> 在每一行使用 \* 不是必要的，但是可以让文档看起来更加整齐，而且 IDE 也常常会自动在每行加上一个 \*。

文档注释中，可以使用 HTML 标签，例如 `<em></em>`、`<strong></strong>`、`<img>`，但是不可以使用 `<h1></h1>`、`<hr>`，另外要插入代码，使用 '{@code ...}'，而不是 `<code>`，这样开发者就不需要对代码中的 \< 进行转义了。

如果文档中需要用到其他文件或者图片的连接，应该把这些文件放到子目录 doc-files 中，这样连接中就可以使用 `<img src="doc-files/demo.jpg">`。

## 通用注释

以下标签可以用在类中的文档注释里：

- @author 作者信息
- @version 版本信息
- @since 始于哪一个版本，常引入一个版本描述
    `@since version 5.2.1`
- @deprecated 废弃信息描述，当一个类或者方法等被废弃了，在此说明取代建议，可以通过 @see 和 @link 标记，连接到文档的其他部分
    `@deprecated Use <code> setVisible(true) </code> instead`
- @see 指向另一个主题的链接，可以用于类或者方法中，这个标记将在“see also”部分增加一个超链接
    ```java
    /**
     * @see com.dong.Test#sayHi(String name)
     */
    ```

    上述文档注释，javadoc 将会在文档中插入一个超链接，链接到 com.dong.Test 类的 sayHi(String name)方法。
    > 可以省略包名，这将使得链接定位至当前包，如果连类名也省略，则会定位至当前类。

    **类名与方法名之间务必使用 # 分割**。

- @link 使用在注释中的任何位置，这将在 @link 的地方产生一个指向其他类或方法的超链接。

## 包注释

为包写注释，有两种方法：

1. 在包中提供一个 package.html 文件。在 `<body></body>` 之间的文本都会被提取出来。

```html
<html>
<body>
包注释 使用Html生成
</body>
</html>
```

2. 在包中提供一个 package-info.java 文件。package-info.java 文件必须以 /** 和 */ 开始，并且在后面紧跟包语句。

```java
/**
 * 包注释 使用 package-info.java 生成
 */
package testjava;
```

也可以为整个项目提供一个概述文件——overview.html，这个文件放在整个项目的根目录下，与使用 Html 生成包注释一样，把内容放置在 `<body></body>` 之间，最终内容将会显示在网页的 Overview 链接中。

## 类注释

类注释必须放在 import 语句之后，类定义之前，格式如下：

```java
package testpackage;

/**
 * 类注释
 */
public class Test {

}
```

## 域注释

一般只需要为 public 修饰的域（一般指静态常量）增加文档注释，例如：

```java
/**
 * 静态常量注释
 */
public static final String word = "Hello world!";
```

## 方法注释

在方法注释中，除了可以使用通用注释的标签，还可以使用以下标签：

- @param 变量描述

    这个标记对当前方法的每个参数进行说明，这个描述可以占据多行，也可以使用 HTML，但是所有 @param 标记必须放到一起。

- @return 返回值描述

    这个标记对当前方法的返回值进行描述，与 @param 一样，可以占据多行也可以使用 HTML。

- @throws 异常类描述

    这个标记对当前方法的可能抛出的异常进行描述。

```java
/**
 * 来和客人打个招呼
 *
 * @param name 要打招呼的人的名字
 * @return 返回打招呼要说的话
 */
public String sayHi(String name) {
    return "Hi " + "name";
}
```

## javadoc 工具使用

### 生成 java doc

如果是一个包，则运行命令：
`javadoc -d docDirectory nameOfPackage`

或者对于多个包，使用：
`javadoc -d docDirectory nameOfPackage1 nameOfPackage2 ...`

如果文件在默认包中，则使用：
`javadoc -d docDirectory *.java`

如果省略 -d docDirectory，则生成的 doc 文件在当前目录下。

### 设置文档编码

对于中文，编码总是令人困扰的，如果代码中使用的是 urf-8，可以在命令中指定编码，防止出现乱码，如下：
`javadoc -d docDirectory nameOfPackage -encoding utf8`

### 抛出 IllegalArgumentException 异常

在使用 javadoc 工具的时候，在生成文档的时候，很多人会遇到 IllegalArgumentException 异常，有人提出在 classpath 中，把对 `JAVA_HOME` 的引用换成绝对路径即可，但是好像不是很管用。

如果确实不起效果，那直接清空 classpath 中的值即可。
