---
title: Java 的 shutdown hook 钩子
date: 2018-08-05 18:00:00
tags:
    - Java
---

## shutdown hook 介绍

在 Java 程序中，有时候我们可能希望在程序执行结束的时候进行一些工作，例如将信息保存，给服务器发送登出记录等。最简单的方式，自然是在执行退出的时候，调用某个方法，但是这样的处理方式存在一定的局限性。当用户使用一些特殊手段将程序关闭的时候，这样的方式就行不通的。

那如何在 Java 程序没有按照我们的意图退出的时候，能够执行那些退出前的工作呢？Java 中提供了 addShutdownHook(Thread hook) 方法，在 Java 虚拟机终止的时候，将会调用此方法。

<!-- more -->

我们知道 Java 程序是运行在 Java 虚拟机上的，当程序启动的时候，会启动一个 Java 虚拟机，当程序结束的时候，这个 Java 虚拟机也就关闭了。Java 虚拟机会在两种情况下关闭：

- 程序正常退出的时候。即最后一个 non-daemon（非守护程序线程退出）或者调用 System.exit() 方法时。
- 虚拟机被用户终止。用户通过 ^c，或者一些系统事件，例如用户注销，关机。

addShutdownHook 方法接受一个初始化的 Thread 类。当虚拟机关闭的时候，就会开始**同时**执行这些被添加的 Thread。另外此时 daemon 线程仍在运行，如果通过调用 System.exit() 退出，则 non-deamon 线程也将继续运行！

一旦开始执行退出序列的工作，就只能通过 halt() 方法强制关闭 Java 虚拟机，而且此时也不能够在增加新的 shutdown hook 钩子，否则将抛出 IllegalStateException。

由于关闭挂钩运行的时刻很特别，所以在编写此部分代码的时候，应该时刻注意，**保证线程安全，避免死锁，不要依赖其他钩子**。

## shuntdown hook 的使用

### shutdown hook 的基本使用

```java
public class HookTest1 {
    public static void main(String[] args) {
        hook();
        System.out.println("Application is running");
    }

    private static void hook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Execute Hook...");
        }));
    }
}
```

程序执行结果：

```console
Application is running
Execute Hook...
```

### shutdown hook 在非正常结束时候的使用

#### 使用 ^c 退出

用户在终端中使用 ^c 可以结束一个程序的执行，在这种情况下，hook 是可以被正常调用的。

```java
public class HookTest2 {
    public static void main(String[] args) {
        hook();
        Thread thread = new Thread(() -> {
            while (true) {
                System.out.println("thread is running....");
                try {
                    TimeUnit.MILLISECONDS.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    private static void hook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Execute Hook...");
        }));
    }
}
```

编译后使用命令行调用，然后使用 ctrl + c，输出一下内容：

```console
thread is running....
thread is running....
thread is running....
thread is running....
Execute Hook...
```

#### 内存溢出时

Java 虚拟机出现内存溢出时，会抛出 OutOfMemoryError，也会导致虚拟机异常关闭，此情况下 addShutdownHook 依然可以起作用。

```java
public class HookTest3 {
    public static void main(String[] args) {
        hook();
        throw new OutOfMemoryError();
    }

    private static void hook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Execute Hook...");
        }));
    }
}
```

代码执行后输出如下：

```
Exception in thread "main" java.lang.OutOfMemoryError
    at hook.HookTest3.main(HookTest3.java:6)
Execute Hook...
```

由上可见，代码是被用户终止也好，还是代码抛出异常退出也好，shutdown hook 中的 Thread 都是可以被正常执行的。在 Linux 中如果执行 `kill pid` 命令 hook 仍能被执行，但是执行 `kill -9 pid` 命令时是不会被执行的，在 Windows 系统中，如果直接在任务管理器中，结束任务，hook 也不会被执行。

> 在 shutdown hook 钩子中，尽可能的执行简单的关闭操作等，避免因为这些钩子造成关闭期间进行长时间的计算。

---

更多详情参阅 [Java Doc](https://docs.oracle.com/javase/10/docs/api/java/lang/Runtime.html#addShutdownHook&#40;java.lang.Thread&#41;)